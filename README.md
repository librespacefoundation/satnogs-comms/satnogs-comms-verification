# SatNOGS COMMS Verification #

This is an issue-only GitLab repository containing test cases for SatNOGS COMMS.

When a new issue is opened and is related to verification, use the Test Case issue
template.

Test cases are running for each PCB version.
For each PCB version create 3 scope labels:
* PCB #number-of-version :: Pass
* PCB #number-of-version :: Fail
* PCB #number-of-version :: No Tested

Use these labels in issues to inform about the results of each test case. If you
want to add detail results for a test case use the [Test-Report document](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc)
in SatNOGS COMMS System Design Document repository.

Add these labels in [templates/include/test-report.tex.j2](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc). An auto-generated
table exists in [Test-Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/blob/master/test-report.tex?ref_type=heads) which is the
source of truth about SatNOGS COMMS verification.
